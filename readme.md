##How To Use It?##
###1.Set Up Html Layout###
``<div class="slide-container">``

``     <div class="slide">1</div>``

``     <div class="slide">2</div>``

``     <div class="slide">3</div>``

``     <div class="slide">4</div>``

``     <div class="slide">5</div>``

``     <div class="slide">6</div>``

``     <div class="slide">7</div>``

``  </div>``

###2.Init Slider Class###

``var myslider = new slider({container:".slide-container",slideMarginRight:20,slideWidth:300,slideBorderWidth:1,siteMiddleContainerWidth:800,animationDuration:500})``


###3.Move Slide###

* move to next slide: ``myslider.moveToNext();``
* move to previous slide: ``myslider.moveToPrev();``
* move to specific slide: ``myslider.scrollToSlide(index);``



