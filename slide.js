/**
* a class to control the slide
**/
var slider=function(config){  
  /**
  * the config file should have the class of
  * container, the number of column
  **/
  try{
   var container = config.container;
   var slideMarginRight= config.slideMarginRight;
   var slideWidth = config.slideWidth;
   var siteMiddleContainerWidth = config.siteMiddleContainerWidth;
   var slideBorderWidth = config.slideBorderWidth;
   if (config.animationDuration) {
     var speed = config.animationDuration;
   }else{
     var speed = 1000;
   }
     
   var that = this;
  }catch(e){
    console.log(e.message);
  }
   
  /**
  * get the slide width
  **/
  this.slideWidth = slideWidth+slideMarginRight+slideBorderWidth*2;

   /**
   * get total slides inside container 
   **/
   this.numberOfSlides = jQuery(container).children("div").length;


   /**
   * function to get page width
   **/
   this.getPageWidth=function(paddingLeft){
       var winW = jQuery(window).width();
       return winW - paddingLeft;
   }

   /**
   * function to get how many slide in one page
   **/
   this.getMaxSlideInOnePage=function(slideWidth,pageWidth){
        return Math.floor(pageWidth/slideWidth);
   };

   /**
   * should init the padding left of container
   * should get screen width and minddle container width
   * if middle container width is smaller then screen width
   * then padding left should equal to (screenwidth-middlewidth)/2
   **/
   
   this.getPaddingLeft=function(siteMiddleContainerWidth){
      var winW = jQuery(window).width();
      if (winW > siteMiddleContainerWidth) {
          var paddingLeft = parseInt((winW - siteMiddleContainerWidth)/2);
       }else{
          var paddingLeft = 0; 
       }
       return paddingLeft;      
   };

   /**
   * function to go to specific slide
   **/
   this.scrollToSlide=function(index){
       var slideNumberOnePage = this.getMaxSlideInOnePage(this.slideWidth,this.pageWidth);
       if (index <= slideNumberOnePage){
         var dis=0;
       }else{
         var slidsToScroll = index - slideNumberOnePage;
         var dis = this.slideWidth*slidsToScroll; 
       }

       /**
       * should update current slide index
       **/
       this.currentSlide = index <= this.numberOfSlides?(index <= this.maxSlideInOnePage?this.maxSlideInOnePage:index): this.numberOfSlides;
       jQuery(container).animate({
        scrollLeft:dis
       },speed,function(){
          /**
          * wehn animation finish, should update the opacity of the slide
          **/
          that.setOpacityForSlides(that.currentSlide,that.maxSlideInOnePage,container);
       });
   };

   /**
   * function to move to next slide
   **/
   this.moveToNext = function(){
      if(this.currentSlide >= this.numberOfSlides) return false;
      this.currentSlide++;
      this.scrollToSlide(this.currentSlide);
   };


   /**
   * function to move to previous slide
   **/
   this.moveToPrev = function(){
       if (this.currentSlide<= this.maxSlideInOnePage) return false;
       this.currentSlide--;
       this.scrollToSlide(this.currentSlide);
   };

   /**
   * function to set opacity to the slide which has been moved to left border
   **/
   this.setOpacityForSlides=function(currentSlide,maxSlideInOnePage,container){
     /**
     * should remove all the slide opacity first
     **/
     jQuery(container).children("div").removeClass("transparency");
     if (currentSlide<=maxSlideInOnePage) return false;
     var numberOfSlidesToChange = currentSlide - maxSlideInOnePage;
     jQuery(container).children("div:lt("+numberOfSlidesToChange+")").addClass("transparency");
   };


   /**
   * function to init the slider
   **/
   this.initSlider = function(noChangeCurrentSlide){
      /**
      * function to get the current slide Number
      **/
      this.paddingLeft = this.getPaddingLeft(siteMiddleContainerWidth);
      this.pageWidth = this.getPageWidth(this.paddingLeft);
      this.maxSlideInOnePage = this.getMaxSlideInOnePage(this.slideWidth,this.pageWidth);      
      if(!noChangeCurrentSlide) this.currentSlide = this.maxSlideInOnePage;
      /**
      * should init the value of padding left for slide container
      **/
      jQuery(container).css("padding-left",this.paddingLeft+"px");  
   }

   this.initSlider();

    /**
    * should add event listener to window resize
    **/
    jQuery(window).on("resize",function(){
         that.initSlider(true);
         that.scrollToSlide(that.currentSlide);
    });
}
